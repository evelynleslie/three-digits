package assignment;
import java.util.*;

public class ThreeDigits implements Comparable<ThreeDigits>{

	public int m_p = 0;
	public int m_c = 0;
	public int m_digitChanged = 0;
	public int m_dcp = 0;
	public int m_g = 0;
	public int m_depth = 0;
	public int m_h = 0;
	public int m_id = 0;
	public int m_ph = 0;
	public ArrayList<ThreeDigits> children = new ArrayList<ThreeDigits>();
	
	public ThreeDigits(int parent, int current, int digitChanged, int goal, int dp, int d, int h, int id, int ph){
		m_p = parent;
		m_c = current;
		m_digitChanged = digitChanged;
		m_g = goal;
		m_dcp = dp;
		m_depth = d;
		m_h = h;
		m_id = id;
		m_ph = ph;
	}
	
	public int compareTo(ThreeDigits other) {
		if(this.m_h != other.m_h){
			return Integer.compare(this.m_h, other.m_h);
		}
		else{
			return Integer.compare(other.m_id, this.m_id);
		}
	}
}