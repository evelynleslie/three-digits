package assignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class IO {
	
	public ArrayList<Integer> m_forbidden;
	public int m_s;
	public int m_g;
	public String m_fileName;
	
	public IO (String fn) {
		this.m_forbidden = new ArrayList<Integer>();
		this.m_s = 0;
		this.m_g= 0;
		this.m_fileName = fn;
	}
	
	public int getS() {
		return m_s;
	}
	
	public int getG() {
		return m_g;
	}
	
	public ArrayList<Integer> getForbidden(){
		return m_forbidden;
	}
	
	public void getParamFromFile() {

		int line = 0;
		String sCurrentLine;

		BufferedReader br = null;

		try {			
			br = new BufferedReader(new FileReader(m_fileName));			
			while ((sCurrentLine = br.readLine()) != null) {
				line++;
				if (line == 1) {
					m_s = Integer.parseInt(sCurrentLine);
				} 				
				else if (line == 2) {
					m_g = Integer.parseInt(sCurrentLine);
				} 
				else {
					String [] temp = sCurrentLine.split(",");					
					for(int i = 0; i < temp.length; i++){
						m_forbidden.add(Integer.parseInt(temp[i]));						
					}
				}
			}
		}

		catch (IOException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (br != null) 
					br.close();
			}
			catch (IOException ex) {
				ex.printStackTrace();
				}
			}
		}
	}
	