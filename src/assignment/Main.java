package assignment;
import assignment.IO;
import assignment.ThreeDigits;
import java.util.*;

public class Main {
	
	public static int m_idAll = 0;
	public static ArrayList<Integer> m_forbidden = new ArrayList<Integer>();
	public static String m_algo;
	
	public static int heuristic(int p, int n){
		int h = 0;		
		int first = Math.abs((p % 1000)/100 - (n % 1000)/100);
		int second = Math.abs((p % 100)/10 - (n % 100)/10);
		int third = Math.abs((p % 10) - (n % 10));		
		h = first + second + third;		
		return h ;
	}
	
	public static boolean checkForbidden(ThreeDigits threedigits){		
		boolean check = true;
		for(int i = 0; i < m_forbidden.size(); i++){
			if(m_forbidden.get(i) == threedigits.m_c){
				check = false;	
			}
		}		
		return check;
	}
	
	public static void printPath (ArrayList<ThreeDigits> expanded){		
		ThreeDigits threedigits = expanded.get(expanded.size()-1);		
		int y = expanded.size()-1;		
		ArrayList<Integer> path = new ArrayList<Integer>();		
		while(threedigits.m_p != -1){
            path.add(threedigits.m_c);
            for(int i = 0; i < y; i++){
                if(expanded.get(i).m_c == threedigits.m_p && expanded.get(i).m_digitChanged == threedigits.m_dcp){
                    threedigits = expanded.get(i);
                    y = i;
                    break;
                }
            }
        }
        path.add(threedigits.m_c);		
		for(int i = path.size()-1; i >= 0; i--){			
			String p = path.get(i).toString();
			print(p);			
			if(i != 0){
				System.out.print(",");
			}
			else{
				System.out.print("\n");
			}
		}		
	}
	
	public static void print (String s){					
			if(s.length() == 1){
				System.out.print("00"+s);
			}
			else if(s.length() == 2){
				System.out.print("0"+s);
			}
			else{
				System.out.print(s);
			}
		
	}
	
	public static boolean checkExpanded (ArrayList<ThreeDigits> e, ThreeDigits threedigits){
		boolean x = true;		
		for(int i = 0; i < e.size(); i++){
			if(threedigits.m_c == 010 && i == 33){
			}
			if(e.get(i).m_c == threedigits.m_c && e.get(i).m_digitChanged == threedigits.m_digitChanged){
				if(threedigits.m_c == 010 && i == 33){
				}
				x = false;
			}
		}		
		return x;
	}
	
	public static ArrayList<ThreeDigits> subExpand(ThreeDigits threedigits, int tmp, ArrayList<ThreeDigits> checkExpanded, int dpth) {
		m_idAll++;
		int h = heuristic(threedigits.m_g,tmp);
		if(m_algo.equals("A")){
			h += threedigits.m_depth;
		}
		ThreeDigits nw = new ThreeDigits(threedigits.m_c,tmp,1,threedigits.m_g,threedigits.m_digitChanged,dpth,h,m_idAll,threedigits.m_h);
		boolean checked = checkForbidden(nw);
		if(checked == true){
			checkExpanded.add(nw);
		}
		else{
			m_idAll--;
		}
		return checkExpanded;
	}
	
	public static ArrayList<ThreeDigits> expand (ThreeDigits threedigits){		
		ArrayList<ThreeDigits> checkExpanded = new ArrayList<ThreeDigits>();
		int dpth = threedigits.m_depth + 1;	
		String c = Integer.toString(threedigits.m_c);
		if(threedigits.m_digitChanged != 1){
			int x = threedigits.m_c % 1000;
			if(c.length() == 3){				
				int tmp = threedigits.m_c - 100;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}
			if(x < 900){
				int tmp = threedigits.m_c + 100;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}
		}		
		if(threedigits.m_digitChanged != 2){
			int x = threedigits.m_c % 100;
			if(c.length() != 1 && x > 9){
				int tmp = threedigits.m_c - 10;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}
			if(x < 90){
				int tmp = threedigits.m_c + 10;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}
		}		
		if(threedigits.m_digitChanged != 3){
			int x = threedigits.m_c % 10;
			if(x != 0){
				int tmp = threedigits.m_c - 1;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}			
			if(x != 9){
				int tmp = threedigits.m_c + 1;
				checkExpanded = subExpand(threedigits, tmp, checkExpanded, dpth);
			}
		}
		return checkExpanded;
	}
	
	public static void BFS(LinkedList<ThreeDigits> q, ArrayList<ThreeDigits> expanded){	
		ThreeDigits curr = q.peek();		
		while(!q.isEmpty()){			
			outOfBound(expanded, expanded);
			ThreeDigits threedigits = q.pop();			
			boolean checkExpanded = checkExpanded(expanded,threedigits);
			if(checkExpanded == true){
				expanded.add(threedigits);
			}			
			if(threedigits.m_c == threedigits.m_g){
				break;
			}			
			threedigits.children = expand(threedigits);			
			for(int i = 0; i < threedigits.children.size(); i++){
					q.add(threedigits.children.get(i));
			}
		}		
		if(expanded.get(expanded.size() -1).m_c != curr.m_g){
			solutionFound(expanded);
		}		
		printPath(expanded);
		for(int j = 0; j < expanded.size(); j++){
			String s = Integer.toString(expanded.get(j).m_c);
			print(s);	
			if(j != expanded.size() - 1){
				System.out.print(",");
				}
			}
		}
	

	public static void DFS(Stack<ThreeDigits> s, ArrayList<ThreeDigits> expanded){		
		ThreeDigits threedigits = s.peek();		
		while(!s.isEmpty()){			
			outOfBound(expanded, expanded);			
			ThreeDigits td= s.peek();			
			addExpanded(expanded, td);						
			if(td.m_c == td.m_g){
				break;
			}			
			boolean checkExpanded = false;			
			for(int i = 0; i < td.children.size(); i++){
				checkExpanded = checkExpanded(expanded,td.children.get(i));
				if(checkExpanded == true){
					s.push(td.children.get(i));
					break;
				}
			}			
			if(checkExpanded == false){
				s.pop();
			}					
		}			
		if(expanded.get(expanded.size() -1).m_c != threedigits.m_g){
			solutionFound(expanded);
		}		
		printPath(expanded);
		for(int j = 0; j < expanded.size(); j++){
			String str = Integer.toString(expanded.get(j).m_c);
			print(str);	
			if(j != expanded.size() - 1){
				System.out.print(",");
				}
			}
		}
	
	
	public static ArrayList<ThreeDigits> addExpanded(ArrayList<ThreeDigits> expanded, ThreeDigits td){
		td.children = expand(td);				
		boolean c = checkExpanded(expanded,td);
		if(c == true){
			expanded.add(td);
		}
		return expanded;
	}
	
	public static void IDS(ArrayList<ThreeDigits> expanded, ThreeDigits td){		
		Stack<ThreeDigits> s = new Stack<ThreeDigits>();
		s.push(td);
		ArrayList<ThreeDigits> e = new ArrayList<ThreeDigits>();
		ThreeDigits threedigits = s.peek();
		int depth  = 0;
		while(depth != -1){										
			outOfBound(expanded, e);
			ThreeDigits n = s.peek();
			if(n.m_depth < depth){
				addExpanded(expanded, n);			
				if(n.m_c == n.m_g){
					break;
				}				
				boolean checkExpanded = false;				
				for(int i = 0; i < n.children.size(); i++){
					checkExpanded = checkExpanded(expanded,n.children.get(i));
					if(checkExpanded == true){
						s.push(n.children.get(i));
						break;
					}
				}				
				if(checkExpanded == false){
					s.pop();
				}
			}
			if(n.m_depth == depth){
				expanded.add(n);
				s.pop();
				if(n.m_c == n.m_g){
					break;
				}
			}			
			if(s.isEmpty()){
				s.push(td);
				depth++;
				e.addAll(expanded);
				expanded.clear();
			}
		}			
		e.addAll(expanded);		
		outOfBound(expanded, e);			
		if(expanded.get(expanded.size() -1).m_c != threedigits.m_g){
			solutionFound(e);
		}		
		printPath(expanded);
		for(int j = 0; j < expanded.size(); j++){
			String str = Integer.toString(expanded.get(j).m_c);
			print(str);	
			if(j != expanded.size() - 1){
				System.out.print(",");
				}
			}
		}
	
	public static void solutionFound(ArrayList<ThreeDigits> e) {
		System.out.println("No solution found");
		for(int j = 0; j < e.size(); j++){
			String s = Integer.toString(e.get(j).m_c);
			print(s);	
			if(j != e.size() - 1){
				System.out.print(",");
				}
			}
		System.exit(0);
	}
	
	public static void outOfBound(ArrayList<ThreeDigits> expanded, ArrayList<ThreeDigits> e) {
		if(e.size() >= 1000){
			System.out.println("No solution found");
			for(int j = 0; j < 1000; j++){
				String str = Integer.toString(e.get(j).m_c);					
				print(str);
				if(j != expanded.size() - 1){
					System.out.print(",");
					}
				}
			System.exit(0);
			}
		}
	
	public static void greedyHillA(PriorityQueue<ThreeDigits> pq, ArrayList<ThreeDigits> expanded){		
		ThreeDigits td = pq.peek();		
		while(!pq.isEmpty()){
			outOfBound(expanded, expanded);
			ThreeDigits threedigits = pq.remove();
			if(m_algo.equals("H")) {
				pq.clear();
				if(threedigits.m_h >= threedigits.m_ph){
					solutionFound(expanded);
				}
			}
			boolean checkExpanded = checkExpanded(expanded,threedigits);
			if(checkExpanded == true){
				expanded.add(threedigits);
			}		
			if(threedigits.m_c == threedigits.m_g){
				break;
			}
			threedigits.children = expand(threedigits);
			for(int i = 0; i < threedigits.children.size(); i++){
					pq.add(threedigits.children.get(i));
			}
		}		
		if(expanded.get(expanded.size() -1).m_c != td.m_g){
			solutionFound(expanded);
		}		
		printPath(expanded);
		for(int j = 0; j < expanded.size(); j++){
			String s = Integer.toString(expanded.get(j).m_c);
			print(s);	
			if(j != expanded.size() - 1){
				System.out.print(",");
			}
		}	
	}
	
	public static void main(String[] args) {
		if (args.length < 1) {
			System.exit(0);
		}		
		IO io = new IO(args[1]);
		String m_algo = args[0];
		ArrayList<ThreeDigits> expanded = new ArrayList<ThreeDigits>();		
		io.getParamFromFile();
		int g = io.getG();
		int s = io.getS();
		m_forbidden = io.getForbidden();		
		int heu = heuristic(g,s);
		ThreeDigits root = new ThreeDigits(-1, s, 0, g,0,0,heu,0,heu+1);		
		if(m_algo.equals("B")){
			LinkedList<ThreeDigits> q = new LinkedList<ThreeDigits>();
			q.add(root);
			BFS(q,expanded);
		}		
		else if(m_algo.equals("D")){
			Stack<ThreeDigits> st = new Stack<ThreeDigits>();
			st.push(root);
			DFS(st,expanded);
		}		
		else if(m_algo.equals("I")){
			IDS(expanded,root);
		}		
		else {
			PriorityQueue<ThreeDigits> pq = new PriorityQueue<ThreeDigits>();
			pq.add(root);			
			greedyHillA(pq,expanded);
		}		
	}
}
